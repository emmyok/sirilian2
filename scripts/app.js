requirejs.config({
    baseUrl: 'scripts/app',
    paths: {
        app: "../app",
        TweenMax: '../lib/TweenMax',
        pixijs: '../lib/pixi'
    }
});

requirejs(["TweenMax", "pixijs"], function() {
    requirejs(["app/main"]);
});
