define(['Column', 'pixi', 'Engine/Mask', 'Symbol', 'AnimatedSymbol'], function (Column, pixi, Mask, Symbol, AnimatedSymbol) {

    function Columns() {
        this.columns = [];
        this.create();
        this.createRandomSymbols();
    }

    Columns.prototype.create = function () {
        var leftPosition = [45, 190, 340, 480, 628];
        for (var i = 0; i < 5; i++) {
            var column = new Column();
            this.mask(column, leftPosition[i]);
            column.create(leftPosition[i]);
            this.columns.push(column);
            pixi.getStage().addChild(column);
        }
    };

    Columns.prototype.createRandomSymbols = function () {

        for (var column = 0; column < this.columns.length; column++) {
            for (var i = 0; i < 3; i++) {
                var symbol = new Symbol();
                symbol.y = 132 * i;
                symbol.create(this.randomNumber());
                this.columns[column].addChild(symbol);
            }
        }
    };

    Columns.prototype.randomNumber = function () {
        return Math.floor((Math.random() * 10) + 1);
    };

    Columns.prototype.changeSymbols = function () {
        var howManytime = 0;
        this.interval = setInterval(function () {
            howManytime++;
            if (howManytime <= 15) {
                for (var column = 0; column < this.columns.length; column++) {
                    for (var i = 0; i < 3; i++) {
                        this.columns[column].children[i].changeSymbol(this.randomNumber())
                    }
                }
            } else {
                clearInterval(this.interval);
                this.finalSymbols();

            }

        }.bind(this), 200);

    }

    Columns.prototype.finalSymbols = function () {
        for (var column = 0; column < this.columns.length; column++) {
            for (var i = 0; i < 3; i++) {
                var randomSymbol = this.randomNumber();
                this.columns[column].children[i].changeSymbol(randomSymbol);
                if (randomSymbol > 3) {
                    setTimeout(function (column, i, randomSymbol) {

                        if (randomSymbol > 9) {
                            var animatedSymbol = new AnimatedSymbol('symbol' + randomSymbol + '_anim_');
                        } else {
                            var animatedSymbol = new AnimatedSymbol('symbol0' + randomSymbol + '_anim_');
                        }
                        animatedSymbol.create(this.columns[column].children[i]);
                    }.bind(this, column, i, randomSymbol), 500 * column);
                }
                // pixi.openSpin()
            }
        }
    };

    Columns.prototype.mask = function (column, x) {
        var mask = new Mask();
        mask.setWhatToMask(column);
        mask.setPosition(x, 100);
        mask.setHeight(396);
        mask.setWidth(132);
        mask.create();
    };






    return Columns;
});