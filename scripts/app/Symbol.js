define(function () {
    function Symbol() {
        PIXI.Sprite.apply(this, arguments);
    }

    Symbol.prototype = Object.create(PIXI.Sprite.prototype);

    Symbol.prototype.create = function (symbolNumber) {

        this.texture = PIXI.Texture.fromFrame('reel_'+symbolNumber);
        this.width = 132;
        this.height = 132;
    }

    Symbol.prototype.changeSymbol = function (symbolNumber) {
        this.texture = PIXI.Texture.fromFrame('reel_'+symbolNumber);
    };

    return Symbol;
});