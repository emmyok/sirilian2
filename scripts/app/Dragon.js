define(['pixi'], function (pixi) {

    function Dragon(fileName) {
        PIXI.extras.AnimatedSprite.call(this, this.getTexture(fileName));
        this.create(fileName);
    }

    Dragon.prototype = Object.create(PIXI.extras.AnimatedSprite.prototype);

    Dragon.prototype.getTexture = function (fileName) {
        var textures = [];
        for (var i = 1; i <= 14; i++) {
            textures.push(PIXI.Texture.fromFrame(fileName + i));
        }
        return textures;
    }

    Dragon.prototype.create = function (fileName) {
        if (fileName === 'logo-left_') {
            this.x = 207;
        } else {
            this.x = 506;
        }
        this.y = 30;
        this.loop = false;
        pixi.getStage().addChild(this);
    }

    return Dragon;

});