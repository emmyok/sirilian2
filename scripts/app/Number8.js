define(['Numbers','Payline'],function (Numbers,Payline) {

    function Number8(){
        Numbers.apply(this,arguments);
        this.create();
        this.payline = new Payline();
        this.payline.create(6, 0, 80);
    };
    Number8.prototype = Object.create(Numbers.prototype);

    Number8.prototype.onMouseOver = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_27');
        this.button.texture = newTexture;
        this.payline.show();
    };

    Number8.prototype.onMouseOut = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_35');
        this.button.texture = newTexture;
        this.payline.hide();
    };

    Number8.prototype.create = function () {
        Numbers.prototype.create.call(this,'numbers_35');
        this.button.x=780;
        this.button.y=257;
        this.button.mouseover = this.onMouseOver.bind(this);
        this.button.mouseout = this.onMouseOut.bind(this);
    };

    return Number8;
});