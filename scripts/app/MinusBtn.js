define(['Button'], function (Button) {
    function MinusBtn(x, y) {
        Button.apply(this, arguments);
        this.create(x, y);
    };

    MinusBtn.prototype = Object.create(Button.prototype);

    MinusBtn.prototype.create = function (x, y) {
        Button.prototype.create.call(this, 'minus_');
        this.button.x = x;
        this.button.y = y;
    }
    return MinusBtn;
});