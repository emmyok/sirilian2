define(function () {
    function Music() {
    };
    Music.prototype.create = function (fileName) {
        this.button = new Audio(fileName);
        this.button.play();
    };

    return Music;
});