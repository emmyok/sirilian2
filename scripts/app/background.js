define(['Engine/Events', 'Engine/Enum', 'pixi', 'Music'], function (events, eNum, pixi, Music) {

    function Background() {
        events.on(eNum.EVENTS.ASSETS_LOADED, this.create);
    };

    Background.prototype.create = function () {
        var sprite = new PIXI.Sprite(PIXI.Texture.fromFrame('bgn.jpg'));
        pixi.getStage().addChild(sprite);

        Music.prototype.create.call(this, 'assets/sounds/theme1.mp3');
        this.button.volume = 0.2;
        this.button.addEventListener('ended', function () {
            this.currentTime = 0;
            this.play();
        }, false);
    };
    return Background;
});