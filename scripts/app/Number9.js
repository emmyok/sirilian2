define(['Numbers', 'Payline'], function (Numbers, Payline) {

    function Number9() {
        Numbers.apply(this, arguments);
        this.create();
        this.payline = new Payline();
        this.payline.create(7, 0, 220);
    }

    Number9.prototype = Object.create(Numbers.prototype);

    Number9.prototype.onMouseOver = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_32');
        this.button.texture = newTexture;
        this.payline.show();
    };

    Number9.prototype.onMouseOut = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_30');
        this.button.texture = newTexture;
        this.payline.hide();
    };

    Number9.prototype.create = function () {
        Numbers.prototype.create.call(this, 'numbers_30');
        this.button.x = 780;
        this.button.y = 300;
        this.button.mouseover = this.onMouseOver.bind(this);
        this.button.mouseout = this.onMouseOut.bind(this);

    };

    return Number9;
});