define(['Chains'], function (Chains) {
    function Chain2() {
        Chains.apply(this, arguments);
        this.create(320, -280);
        this.maskChain(320);
    }

    Chain2.prototype = Object.create(Chains.prototype);

    return Chain2;
});