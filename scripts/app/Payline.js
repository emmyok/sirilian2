define(['pixi'], function (pixi) {

    function Payline() {
        PIXI.Sprite.apply(this, arguments);
    };

    Payline.prototype = Object.create(PIXI.Sprite.prototype);

    Payline.prototype.create = function (frameNumber, x, y) {
        this.texture = PIXI.Texture.fromFrame('paylines_' + frameNumber);
        this.visible = false;
        this.x = x;
        this.y = y;
        pixi.getStage().addChild(this);
    };

    Payline.prototype.show = function (x, y) {
        this.visible = true;
    };

    Payline.prototype.hide = function () {
        this.visible = false;
    };

    return Payline;
})