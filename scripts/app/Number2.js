define(['Numbers', 'Payline'], function (Numbers, Payline) {

    function Number2() {
        Numbers.apply(this, arguments);
        this.create();
        this.payline = new Payline();
        this.payline.create(1, 0, 0);
    };

    Number2.prototype = Object.create(Numbers.prototype);

    Number2.prototype.onMouseOver = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_6');
        this.button.texture = newTexture;
        this.payline.show();
    };

    Number2.prototype.onMouseOut = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_5');
        this.button.texture = newTexture;
        this.payline.hide();
    };

    Number2.prototype.create = function () {
        Numbers.prototype.create.call(this, 'numbers_5');
        this.button.x = 0;
        this.button.y = 118;
        this.button.mouseover = this.onMouseOver.bind(this);
        this.button.mouseout = this.onMouseOut.bind(this);
    };

    return Number2;
});