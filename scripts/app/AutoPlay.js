define(['Button', 'Music'], function (Button, Music) {
    function AutoPlay() {
        Button.apply(this, arguments);
        this.create();
    }

    AutoPlay.prototype = Object.create(Button.prototype);

    AutoPlay.prototype.create = function () {
        Button.prototype.create.call(this, 'autoplay_');
        this.button.x = 238;
        this.button.y = 30;
        this.button.click = function () {
            Music.prototype.create.call(this, 'assets/sounds/auto.mp3');
        };
    };

    return AutoPlay;
});