define(['Numbers', 'Payline'], function (Numbers, Payline) {

    function Number7() {
        Numbers.apply(this, arguments);
        this.create();
        this.payline = new Payline();
        this.payline.create(5, 0, 210);
    };

    Number7.prototype = Object.create(Numbers.prototype);
    Number7.prototype.onMouseOver = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_26');
        this.button.texture = newTexture;
        this.payline.show();
    };

    Number7.prototype.onMouseOut = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_25');
        this.button.texture = newTexture;
        this.payline.hide();
    };

    Number7.prototype.create = function () {
        Numbers.prototype.create.call(this, 'numbers_25');
        this.button.x = 0;
        this.button.y = 395;
        this.button.mouseover = this.onMouseOver.bind(this);
        this.button.mouseout = this.onMouseOut.bind(this);
    };

    return Number7;
});