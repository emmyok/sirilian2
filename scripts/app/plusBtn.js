define(['Button'], function (Button) {

    function Plusbtn(x, y) {
        Button.apply(this, arguments);
        this.create(x, y);
    };

    Plusbtn.prototype = Object.create(Button.prototype);

    Plusbtn.prototype.create = function (x, y) {
        Button.prototype.create.call(this, 'plus_');
        this.button.x = x;
        this.button.y = y;
    };

    return Plusbtn;
});