define(['pixi'],function (pixi) {
    function AnimatedSymbol(fileName) {
        this.frames = {
            symbol04_anim_: 28,
            symbol05_anim_: 18,
            symbol06_anim_: 24,
            symbol07_anim_: 17,
            symbol08_anim_: 56,
            symbol09_anim_: 46,
            symbol10_anim_: 61,
            symbol11_anim_: 28
        };
        PIXI.extras.AnimatedSprite.call(this, this.getTexture(fileName));
    }

    AnimatedSymbol.prototype = Object.create(PIXI.extras.AnimatedSprite.prototype);

    AnimatedSymbol.prototype.getTexture = function (fileName) {
        var textures = [];
        for (var i = 1; i <= this.frames[fileName]; i++) {
            textures.push(PIXI.Texture.fromFrame(fileName + i));
        }
        return textures;
    };

    AnimatedSymbol.prototype.create = function (symbol) {
        this.loop = false;
        symbol.addChild(this);
        this.onComplete = this.clear.bind(this, symbol);
        this.play();
        setTimeout(function () {
            this.onComplete = this.openSpinAgain()
        }.bind(this),2500)

    };
    AnimatedSymbol.prototype.clear = function (symbol) {
        symbol.removeChild(this);
    };

    AnimatedSymbol.prototype.openSpinAgain = function (symbol) {
        pixi.openSpin()
    };

    return AnimatedSymbol;
});