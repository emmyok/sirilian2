define(['Chains'], function (Chains) {
    function Chain4() {
        Chains.apply(this, arguments);
        this.create(610, -280);
        this.maskChain(610);
    };

    Chain4.prototype = Object.create(Chains.prototype);

    return Chain4;
});