define(['pixi', 'Engine/Loader', 'Engine/Events', 'Engine/Enum', 'background', 'buttons', 'Columns'],
    function (pixi, Loader, events, eNum, Background, Buttons, Columns) {
        function init() {
            pixi.init();
            new Background();
            var loader = new Loader();
            loader.setAssetsFromFile('data/assets.json');
            events.on(eNum.EVENTS.ASSETS_FILE_LOADED, loader.load.bind(loader));
            events.on(eNum.EVENTS.ASSETS_LOADED, ready);
        }

        function ready() {
            var columns = new Columns();
            pixi.setColumns(columns);
            new Buttons();
        }

        init();
    });