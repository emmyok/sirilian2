define(['Numbers', 'Payline'], function (Numbers, Payline) {

    function Number4() {
        Numbers.apply(this, arguments);
        this.create();
        this.payline = new Payline();
        this.payline.create(2, 0, 140);
    };

    Number4.prototype = Object.create(Numbers.prototype);

    Number4.prototype.onMouseOver = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_14');
        this.button.texture = newTexture;
        this.payline.show();
    };

    Number4.prototype.onMouseOut = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_13');
        this.button.texture = newTexture;
        this.payline.hide();
    };

    Number4.prototype.create = function () {
        Numbers.prototype.create.call(this, 'numbers_13');
        this.button.x = 780;
        this.button.y = 145;
        this.button.mouseover = this.onMouseOver.bind(this);
        this.button.mouseout = this.onMouseOut.bind(this);
    };

    return Number4;
});