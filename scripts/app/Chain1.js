define(['Chains'], function (Chains) {
    function Chain1() {
        Chains.apply(this, arguments);
        this.create(175, -280);
        this.maskChain(175);
    }

    Chain1.prototype = Object.create(Chains.prototype);

    return Chain1;
});