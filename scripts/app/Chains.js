define(['pixi', 'Engine/Mask'], function (pixi, Mask) {

    function Chains() {
        PIXI.Sprite.apply(this, arguments);
    }

    Chains.prototype = Object.create(PIXI.Sprite.prototype);

    Chains.prototype.create = function (x, y) {
        this.texture = PIXI.Texture.fromFrame('chain');
        pixi.getStage().addChild(this);
        this.x = x;
        this.y = y;
    };

    Chains.prototype.maskChain = function (x) {
        var mask = new Mask();
        mask.setWhatToMask(this);
        mask.setPosition(x, 100);
        mask.setHeight(396);
        mask.setWidth(18);
        mask.create();
    };

    return Chains;
});