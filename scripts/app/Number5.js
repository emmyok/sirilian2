define(['Numbers', 'Payline'], function (Numbers, Payline) {

    function Number5() {
        Numbers.apply(this, arguments);
        this.create();
        this.payline = new Payline();
        this.payline.create(3, 0, 190);
    };

    Number5.prototype = Object.create(Numbers.prototype);

    Number5.prototype.onMouseOver = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_18');
        this.button.texture = newTexture;
        this.payline.show();
    };

    Number5.prototype.onMouseOut = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_17');
        this.button.texture = newTexture;
        this.payline.hide();
    };

    Number5.prototype.create = function () {
        Numbers.prototype.create.call(this, 'numbers_17');
        ;
        this.button.x = 780;
        this.button.y = 420;
        this.button.mouseover = this.onMouseOver.bind(this);
        this.button.mouseout = this.onMouseOut.bind(this);
    };

    return Number5;
});