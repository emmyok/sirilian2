define(['Numbers', 'Payline'], function (Numbers, Payline) {

    function Number1() {
        Numbers.apply(this, arguments);
        this.create();
        this.payline = new Payline();
        this.payline.create(1, 0, 160);
    };

    Number1.prototype = Object.create(Numbers.prototype);

    Number1.prototype.onMouseOver = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_2');
        this.button.texture = newTexture;
        this.payline.show();
    };

    Number1.prototype.onMouseOut = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_1');
        this.button.texture = newTexture;
        this.payline.hide();
    };

    Number1.prototype.create = function () {
        Numbers.prototype.create.call(this, 'numbers_1');
        this.button.x = 0;
        this.button.y = 280;
        this.button.mouseover = this.onMouseOver.bind(this);
        this.button.mouseout = this.onMouseOut.bind(this);
    };

    return Number1;
});