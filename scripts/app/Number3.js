define(['Numbers', 'Payline'], function (Numbers, Payline) {

    function Number3() {
        Numbers.apply(this, arguments);
        this.create();
        this.payline = new Payline();
        this.payline.create(1, 0, 320);
    };

    Number3.prototype = Object.create(Numbers.prototype);

    Number3.prototype.onMouseOver = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_10');
        this.button.texture = newTexture;
        this.payline.show();
    };

    Number3.prototype.onMouseOut = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_9');
        this.button.texture = newTexture;
        this.payline.hide();
    };

    Number3.prototype.create = function () {
        Numbers.prototype.create.call(this, 'numbers_9');
        this.button.x = 0;
        this.button.y = 442;
        this.button.mouseover = this.onMouseOver.bind(this);
        this.button.mouseout = this.onMouseOut.bind(this);
    };
    return Number3;
});