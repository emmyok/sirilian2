define(['Chains'], function (Chains) {
    function Chain3() {
        Chains.apply(this, arguments);
        this.create(470, -280);
        this.maskChain(470);
    };

    Chain3.prototype = Object.create(Chains.prototype);

    return Chain3;
});