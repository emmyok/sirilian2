define(['pixi'], function (pixi) {

    function createPayline(visibility) {
        var texture = PIXI.Texture.fromFrame('paylines_1');
        var sprite = new PIXI.Sprite(texture);
        pixi.getStage().addChild(sprite);
        sprite.x = 0;
        sprite.y = 0;
        sprite.visibility = visibility;
    };

    return {
        createPayline: createPayline
    };
});