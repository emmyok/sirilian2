define(function () {

    function Column() {
        PIXI.Container.apply(this, arguments);
    };

    Column.prototype = Object.create(PIXI.Container.prototype);

    Column.prototype.create = function (x) {
        this.x = x;
        this.y = 100;
    };

    return Column;
});