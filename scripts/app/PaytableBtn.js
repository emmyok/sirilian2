define(['Button', 'Music'], function (Button, Music) {
    function Paytable() {
        Button.apply(this, arguments);
        this.create();
    };

    Paytable.prototype = Object.create(Button.prototype);

    Paytable.prototype.create = function () {
        Button.prototype.create.call(this, 'paytable_button_');
        this.button.x = 715;
        this.button.y = 44;

        this.button.click = function () {
            Music.prototype.create.call(this, 'assets/sounds/paytable_nav_button.mp3');
        };
    };

    return Paytable;

});