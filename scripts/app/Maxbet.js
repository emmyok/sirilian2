define(['Button', 'Music'], function (Button, Music) {
    function Maxbet() {
        Button.apply(this, arguments);
        this.create();
    };

    Maxbet.prototype = Object.create(Button.prototype);

    Maxbet.prototype.create = function () {
        Button.prototype.create.call(this, 'max_bet_');
        this.button.x = 442;
        this.button.y = 30;
        this.button.click = function () {
            Music.prototype.create.call(this, 'assets/sounds/max_bet.mp3');
        }
    };

    return Maxbet;
});