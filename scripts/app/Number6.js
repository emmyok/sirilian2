define(['Numbers', 'Payline'], function (Numbers, Payline) {

    function Number6() {
        Numbers.apply(this, arguments);
        this.create();
        this.payline = new Payline();
        this.payline.create(4, 0, 100);
    };

    Number6.prototype = Object.create(Numbers.prototype);

    Number6.prototype.onMouseOver = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_22');
        this.button.texture = newTexture;
        this.payline.show();
    };
    Number6.prototype.onMouseOut = function () {
        var newTexture = PIXI.Texture.fromFrame('numbers_21');
        this.button.texture = newTexture;
        this.payline.hide();
    };

    Number6.prototype.create = function () {
        Numbers.prototype.create.call(this, 'numbers_21');
        this.button.x = 0;
        this.button.y = 160;
        this.button.mouseover = this.onMouseOver.bind(this);
        this.button.mouseout = this.onMouseOut.bind(this);
    };

    return Number6;
});