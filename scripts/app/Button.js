define(function () {
    function Button() {
        PIXI.Container.apply(this, arguments);
    }

    Button.prototype = Object.create(PIXI.Container.prototype);

    Button.prototype.mouseDown = function () {
        this.gotoAndStop(1);
    };

    Button.prototype.mouseUp = function () {
        this.gotoAndStop(2);
    };

    Button.prototype.mouseOver = function () {
        this.gotoAndStop(0);
    };

    Button.prototype.mouseOut = function () {
        this.gotoAndStop(2);
    };

    Button.prototype.create = function (spriteName) {
        var textures = [];
        for (var index = 1; index <= 3; index++) {
            textures.push(PIXI.Texture.fromFrame(spriteName + index));
        }
        this.button = new PIXI.extras.AnimatedSprite(textures);
        this.button.interactive = true;
        this.button.loop = false;
        this.button.gotoAndStop(2);

        this.button.mousedown = this.mouseDown;
        this.button.mouseup = this.mouseUp;
        this.button.mouseover = this.mouseOver;
        this.button.mouseout = this.mouseOut;
        this.addChild(this.button);
    };

    return Button;
});
