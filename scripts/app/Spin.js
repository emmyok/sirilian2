define(['pixi', 'Button', 'Music', 'Dragon', 'Chain1', 'Chain2', 'Chain3', 'Chain4','Columns'], function (pixi, Button, Music, Dragon, Chain1, Chain2, Chain3, Chain4,Columns) {
    function Spin() {
        Button.apply(this, arguments);
        this.create();
        this.leftDragon = new Dragon('logo-left_');
        this.rightDragon = new Dragon('logo-right_');
        this.createChains();
    };

    Spin.prototype = Object.create(Button.prototype);

    Spin.prototype.create = function () {
        Button.prototype.create.call(this, 'spin_button_');
        this.button.x = 358;
        this.button.y = 0;
        this.button.click = function (e) {
            if(pixi.ableToSpin()){
                console.log(5555)
                pixi.disableSpin()
                Music.prototype.create.call(this, 'assets/sounds/spin.mp3');
                this.leftDragon.gotoAndPlay(0);
                this.rightDragon.gotoAndPlay(0);
                TweenMax.to(this.chain1, 2, {y: 90, yoyo: true, repeat: 1});
                TweenMax.to(this.chain2, 2, {y: 90, yoyo: true, repeat: 1, delay: 0.5});
                TweenMax.to(this.chain3, 2, {y: 90, yoyo: true, repeat: 1, delay: 1});
                TweenMax.to(this.chain4, 2, {y: 90, yoyo: true, repeat: 1, delay: 1.5});
                pixi.getColumns().changeSymbols();
            }
        }.bind(this);
    };

    Spin.prototype.createChains = function () {
        this.chain1 = new Chain1();
        this.chain2 = new Chain2();
        this.chain3 = new Chain3();
        this.chain4 = new Chain4();
    };

    return Spin;
});