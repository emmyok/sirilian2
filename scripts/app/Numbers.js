define(['pixi'], function (pixi) {

    function Numbers() {
        PIXI.Container.apply(this, arguments);
    };

    Numbers.prototype = Object.create(PIXI.Container.prototype);

    Numbers.prototype.create = function (fileName) {
        this.button = new PIXI.Sprite(PIXI.Texture.fromFrame(fileName));
        this.button.interactive = true;
        this.button.buttonMode = true;
        this.button.mouseover = this.mouseOver;
        pixi.getStage().addChild(this.button);
    };

    return Numbers;
});