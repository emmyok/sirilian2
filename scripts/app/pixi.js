define(function () {
    var isAbleToSpin=true;
    var renderer;
    var stage;
    var columns;

    function gameLoop() {
        requestAnimationFrame(gameLoop);
        renderer.render(stage);
    }

    function init() {
        renderer = PIXI.autoDetectRenderer(800, 600, {
            resolution: 1,
            autoResize: true
        });
        document.body.appendChild(renderer.view);
        stage = new PIXI.Container();

        gameLoop();
    };

    function getStage() {
        return stage;
    };

    function getColumns() {
        return columns;
    };

    function setColumns(c) {
       columns=c
    };

    function ableToSpin(){
        return isAbleToSpin
    }

    function disableSpin() {
        isAbleToSpin = false;
    }
    function openSpin() {
        isAbleToSpin = true;
    }

    return {
        init: init,
        getStage: getStage,
        getColumns: getColumns,
        setColumns: setColumns,
        ableToSpin:ableToSpin,
        disableSpin:disableSpin,
        openSpin:openSpin
    };
});