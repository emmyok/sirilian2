define(['Engine/Events', 'Engine/Enum', 'pixi', 'Spin', 'PaytableBtn', 'MinusBtn', 'plusBtn',
        'AutoPlay', 'Maxbet', 'Number1', 'Number2', 'Number3', 'Number4', 'Number5', 'Number6',
        'Number7', 'Number8', 'Number9'],
    function (events, eNum, pixi, Spin, Paytable, MinusBtn, PlusBtn, AutoPlay, Maxbet, Number1,
              Number2, Number3, Number4, Number5, Number6, Number7, Number8, Number9) {

        var btnContainer = new PIXI.Container();
        btnContainer.x = 0;
        btnContainer.y = 495;
        btnContainer.height = 120;
        btnContainer.width = 600;

        function Buttons() {
            this.init();
        }

        Buttons.prototype.init = function () {
            this.bar();
            this.createBtnsAndMusic();
            this.createNumbers();

        };

        Buttons.prototype.createBtnsAndMusic = function () {
            var spinBtn = new Spin();
            var paytableBtn = new Paytable();
            var minusBtn1 = new MinusBtn(14, 48);
            var minusBtn2 = new MinusBtn(130, 48);
            var minusBtn3 = new MinusBtn(578, 48);
            var plusBtn1 = new PlusBtn(90, 48);
            var plusBtn2 = new PlusBtn(205, 48);
            var plusBtn3 = new PlusBtn(683, 48);
            var autoplay = new AutoPlay();
            var maxbet = new Maxbet();

            btnContainer.addChild(spinBtn);
            btnContainer.addChild(paytableBtn);
            btnContainer.addChild(minusBtn1);
            btnContainer.addChild(minusBtn2);
            btnContainer.addChild(minusBtn3);
            btnContainer.addChild(plusBtn1);
            btnContainer.addChild(plusBtn2);
            btnContainer.addChild(plusBtn3);
            btnContainer.addChild(autoplay);
            btnContainer.addChild(maxbet);
        };

        Buttons.prototype.createNumbers = function () {
            var number1 = new Number1();
            var number2 = new Number2();
            var number3 = new Number3();
            var number4 = new Number4();
            var number5 = new Number5();
            var number6 = new Number6();
            var number7 = new Number7();
            var number8 = new Number8();
            var number9 = new Number9();

            pixi.getStage().addChild(number1);
            pixi.getStage().addChild(number2);
            pixi.getStage().addChild(number3);
            pixi.getStage().addChild(number4);
            pixi.getStage().addChild(number5);
            pixi.getStage().addChild(number6);
            pixi.getStage().addChild(number7);
            pixi.getStage().addChild(number8);
            pixi.getStage().addChild(number9);
        };


        Buttons.prototype.bar = function () {
            pixi.getStage().addChild(btnContainer);
            var mainBar = new PIXI.Sprite(PIXI.Texture.fromFrame('main_bar_1'));
            btnContainer.addChild(mainBar);
        };

        return Buttons;
    });